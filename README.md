# Ah, shwmae!

Dw i'n Genevieve Clifford, I'm a PhD student (and learning demonstrator) in Computer Science at [Swansea University](https://swansea.ac.uk), I study digital poverty in the Welsh transgender community. You can find out more at [mun-tonsi.net/phd](https://mun-tonsi.net/phd) (that's a permalink).

## About my GitLab profile
I tend to use GitLab to work on personal projects; see my [GitHub profile](https://github.com/mun-tonsi) for projects where I've collaborated with other people. If you need to get in touch with me, my contact details can be found at [mun-tonsi.net/contact](https://mun-tonsi.net/contact).
